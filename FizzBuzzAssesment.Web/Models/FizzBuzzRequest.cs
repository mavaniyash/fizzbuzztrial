﻿namespace FizzBuzzAssesment.Web.Models
{
    using System.ComponentModel.DataAnnotations;
    using FizzBuzz.Models;
    using PagedList;

    /// <summary>
    /// Class for fiz buzz request model.
    /// </summary>
    public class FizzBuzzRequest
    {
        /// <summary>
        /// Gets or sets number.
        /// </summary>
        [Required(ErrorMessage = "Number is required")]
        [Range(1, 1000, ErrorMessage = "Number should be between 1 to 1000")]
        public int? Number { get; set; }

        /// <summary>
        /// Gets or sets fizzBuzzListValue.
        /// </summary>
        public IPagedList<FizzBuzzModel> FizzBuzzList { get; set; }
    }
}