﻿namespace FizzBuzzAssesment.Web.Controllers
{
    using System.Web.Mvc;

    /// <summary>
    /// Class for Error Controller.
    /// </summary>
    public class ErrorController : Controller
    {
        // GET: Error

        /// <summary>
        /// Index action nmethod to return view.
        /// </summary>
        /// <returns>View.</returns>
        public ActionResult Index()
        {
            return this.View("CustomError");
        }

        /// <summary>
        /// Action method to return custom error view.
        /// </summary>
        /// <returns>View.</returns>
        public ActionResult CustomError()
        {
            return this.View();
        }
    }
}