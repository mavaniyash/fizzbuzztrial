﻿namespace FizzBuzzAssesment.Web.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzz.Business.Interface;
    using FizzBuzz.Common.Helpers;
    using FizzBuzz.Models;
    using FizzBuzzAssesment.Web.Models;
    using PagedList;

    /// <summary>
    /// Controller for fizz buzz.
    /// </summary>
    public class FizzBuzzController : Controller
    {
        private static List<FizzBuzzModel> fizzBuzzList;
        private readonly IFizzBuzz fizzBuzz;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzController"/> class.
        /// </summary>
        /// <param name="fizzBuzz">Interface for IFizzBuzz</param>
        public FizzBuzzController(IFizzBuzz fizzBuzz)
        {
            this.fizzBuzz = fizzBuzz;
        }

        /// <summary>
        /// Action method to review Index view.
        /// </summary>
        /// <returns>View.</returns>
        public ActionResult Index()
        {
            return this.View(new FizzBuzzRequest());
        }

        /// <summary>
        /// Action method to get the list of generated fizz buzz number list.
        /// </summary>
        /// <param name="model">Request model.</param>
        /// <param name="page">Page number.</param>
        /// <returns>List of generated numbers.</returns>
        [HttpPost]
        public ActionResult GenerateFizzBuzz(FizzBuzzRequest model, int page = 1)
        {
            if (this.ModelState.IsValid)
            {
                fizzBuzzList = this.fizzBuzz.GetNumbers(model.Number.Value);
                model.FizzBuzzList = this.GetPagedList(page);
            }

            return this.View("Index", model);
        }

        /// <summary>
        /// Action method to get the pagination fizz buzz generated data.
        /// </summary>
        /// <param name="number">Input number.</param>
        /// <param name="page">Page number.</param>
        /// <returns>List of generated number list.</returns>
        [HttpGet]
        public ActionResult GenerateFizzBuzzPagedData(int number, int page)
        {
            FizzBuzzRequest resModel = new FizzBuzzRequest
            {
                Number = number,
                FizzBuzzList = this.GetPagedList(page),
            };
            return this.View("Index", resModel);
        }

        /// <summary>
        /// Method used to get paged list data.
        /// </summary>
        /// <param name="page">Page number.</param>
        /// <returns>Paged list data.</returns>
        [NonAction]
        protected IPagedList<FizzBuzzModel> GetPagedList(int page)
        {
            return fizzBuzzList.ToPagedList(page, Constants.MaxPageSize);
        }
    }
}