﻿namespace FizzBuzzAssesment.Web
{
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// Class for Route Config.
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Method to Register routes.
        /// </summary>
        /// <param name="routes">Route collection.</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "FizzBuzz", action = "Index", id = UrlParameter.Optional });
        }
    }
}
