﻿namespace FizzBuzzAssesment.Web
{
    using System.Web.Mvc;

    /// <summary>
    /// Class for filter config.
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Method to register global filters.
        /// </summary>
        /// <param name="filters">Global filter collection.</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            // filters.Add(new HandleErrorAttribute());
        }
    }
}
