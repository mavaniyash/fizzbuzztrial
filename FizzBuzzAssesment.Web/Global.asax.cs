﻿namespace FizzBuzzAssesment.Web
{
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using FizzBuzzAssesment.Web.Helpers;

    /// <summary>
    /// Main class for MVC application.
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Application start method.
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        /// <summary>
        /// Application error method.
        /// </summary>
        protected void Application_Error()
        {
            var ex = this.Server.GetLastError();
            ErrorLog.WriteError(ex, "Error");
        }
    }
}
