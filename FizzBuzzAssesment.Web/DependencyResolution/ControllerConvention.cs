namespace FizzBuzzAssesment.Web.DependencyResolution
{
    using System;
    using System.Web.Mvc;

    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;
    using StructureMap.Pipeline;
    using StructureMap.TypeRules;

    /// <summary>
    /// Class for Controller Convention.
    /// </summary>
    public class ControllerConvention : IRegistrationConvention
    {
        /// <summary>
        /// Method for processing.
        /// </summary>
        /// <param name="type">Type.</param>
        /// <param name="registry">Registry.</param>
        public void Process(Type type, Registry registry)
        {
            if (type.CanBeCastTo<Controller>() && !type.IsAbstract)
            {
                registry.For(type).LifecycleIs(new UniquePerRequestLifecycle());
            }
        }
    }
}