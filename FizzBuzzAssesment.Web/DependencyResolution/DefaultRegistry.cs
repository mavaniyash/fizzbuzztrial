// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FizzBuzzAssesment.Web.DependencyResolution
{
    using System;
    using System.Configuration;

    using FizzBuzz.Business.Interface;
    using FizzBuzz.Business.Service;
    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;

    /// <summary>
    /// Class for default registring for dependency.
    /// </summary>
    public class DefaultRegistry : Registry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultRegistry"/> class.
        /// </summary>
        public DefaultRegistry()
        {
            this.Scan(
                scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.With(new ControllerConvention());
                });
            this.For<IFizzBuzz>().Use<FizzBuzz>();
            this.For<ILogicService>().Use<CheckDivisibilityByThree>();
            this.For<ILogicService>().Use<CheckDivisibilityByFive>();
            this.For<ILogicService>().Use<CheckDivisibilityByThreeAndFive>();
            this.For<ICheckDayOfWeek>().Use<CheckDayOfWeek>().Ctor<string>().Is(Convert.ToString(ConfigurationManager.AppSettings["DefaultDayOfWeek"]));
        }
    }
}