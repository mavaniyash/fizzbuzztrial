﻿namespace FizzBuzzAssesment.Web.Helpers
{
    using System;
    using Serilog;
    using Serilog.Events;

    /// <summary>
    /// Static class for error log.
    /// </summary>
    public static class ErrorLog
    {
        private static readonly ILogger Errorlog;
        private static readonly ILogger Warninglog;
        private static readonly ILogger Debuglog;
        private static readonly ILogger Verboselog;
        private static readonly ILogger Fatallog;

        static ErrorLog()
        {
            Errorlog = new LoggerConfiguration()
                .MinimumLevel.Error()
                .WriteTo.File(
                    System.Web.Hosting.HostingEnvironment.MapPath("~/ErrorLog/Error/log.txt"),
                    rollingInterval: RollingInterval.Day,
                    fileSizeLimitBytes: 5242880,
                    rollOnFileSizeLimit: true)
                .CreateLogger();

            Warninglog = new LoggerConfiguration()
                .MinimumLevel.Warning()
                .WriteTo.File(
                    System.Web.Hosting.HostingEnvironment.MapPath("~/ErrorLog/Warning/log.txt"),
                    rollingInterval: RollingInterval.Day,
                    fileSizeLimitBytes: 5242880,
                    rollOnFileSizeLimit: true)
                .CreateLogger();

            Debuglog = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File(
                    System.Web.Hosting.HostingEnvironment.MapPath("~/ErrorLog/Debug/log.txt"),
                    rollingInterval: RollingInterval.Day,
                    fileSizeLimitBytes: 5242880,
                    rollOnFileSizeLimit: true)
                .CreateLogger();

            Verboselog = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.File(
                    System.Web.Hosting.HostingEnvironment.MapPath("~/ErrorLog/Verbose/log.txt"),
                    rollingInterval: RollingInterval.Day,
                    fileSizeLimitBytes: 5242880,
                    rollOnFileSizeLimit: true)
                .CreateLogger();

            Fatallog = new LoggerConfiguration()
                .MinimumLevel.Fatal()
                .WriteTo.File(
                    System.Web.Hosting.HostingEnvironment.MapPath("~/ErrorLog/Fatal/log.txt"),
                    rollingInterval: RollingInterval.Day,
                    fileSizeLimitBytes: 5242880,
                    rollOnFileSizeLimit: true)
                .CreateLogger();
        }

        /// <summary>
        /// Method to write error type log.
        /// </summary>
        /// <param name="ex">Exception.</param>
        /// <param name="message">Message.</param>
        public static void WriteError(Exception ex, string message)
        {
            // Error - indicating a failure within the application or connected system
            Errorlog.Write(LogEventLevel.Error, ex, message);
        }

        /// <summary>
        /// Method to write warning type log.
        /// </summary>
        /// <param name="ex">Exception.</param>
        /// <param name="message">Message.</param>
        public static void WriteWarning(Exception ex, string message)
        {
            // Warning - indicators of possible issues or service / functionality degradation
            Warninglog.Write(LogEventLevel.Warning, ex, message);
        }

        /// <summary>
        /// Method to write debug log.
        /// </summary>
        /// <param name="ex">Exception.</param>
        /// <param name="message">Message.</param>
        public static void WriteDebug(Exception ex, string message)
        {
            // Debug - internal control flow and diagnostic state dumps to facilitate
            Debuglog.Write(LogEventLevel.Debug, ex, message);
        }

        /// <summary>
        /// Method to write verbose log.
        /// </summary>
        /// <param name="ex">Exception.</param>
        /// <param name="message">Message.</param>
        public static void WriteVerbose(Exception ex, string message)
        {
            // Verbose - tracing information and debugging minutiae
            Verboselog.Write(LogEventLevel.Verbose, ex, message);
        }

        /// <summary>
        /// Method to write fatal log.
        /// </summary>
        /// <param name="ex">Exception.</param>
        /// <param name="message">Message.</param>
        public static void WriteFatal(Exception ex, string message)
        {
            // Fatal - critical errors causing complete failure of the application
            Fatallog.Write(LogEventLevel.Fatal, ex, message);
        }

        /// <summary>
        /// Method to write information type log.
        /// </summary>
        /// <param name="ex">Exception.</param>
        /// <param name="message">Message.</param>
        public static void WriteInformation(Exception ex, string message)
        {
            // Fatal - critical errors causing complete failure of the application
            Fatallog.Write(LogEventLevel.Fatal, ex, message);
        }
    }
}