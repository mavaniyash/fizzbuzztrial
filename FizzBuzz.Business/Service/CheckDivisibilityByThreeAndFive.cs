﻿namespace FizzBuzz.Business.Service
{
    using global::FizzBuzz.Business.Interface;
    using global::FizzBuzz.Common.Helpers;

    /// <summary>
    /// Class for divisibility by three and five checking.
    /// </summary>
    public class CheckDivisibilityByThreeAndFive : ILogicService
    {
        private readonly ICheckDayOfWeek checkDayOfWeek;

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckDivisibilityByThreeAndFive"/> class.
        /// </summary>
        /// <param name="checkDayOfWeek">Interface for ICheckDayOfWeek</param>
        public CheckDivisibilityByThreeAndFive(ICheckDayOfWeek checkDayOfWeek)
        {
            this.checkDayOfWeek = checkDayOfWeek;
        }

        /// <summary>
        /// Method used to check if the number if divisibility by three and five.
        /// </summary>
        /// <param name="numberToCheck">Number to check.</param>
        /// <returns>True/False.</returns>
        public bool CheckDivisibility(int numberToCheck)
        {
            return (numberToCheck % Constants.FizzNumber == 0) && (numberToCheck % Constants.BuzzNumber == 0);
        }

        /// <summary>
        /// Method used to return the generated output.
        /// </summary>
        /// <returns>Generated output.</returns>
        public string Output()
        {
            return this.checkDayOfWeek.IsDayMatched() ? string.Concat(Constants.Wizz, ' ', Constants.Wuzz) : string.Concat(Constants.Fizz, ' ', Constants.Buzz);
        }
    }
}
