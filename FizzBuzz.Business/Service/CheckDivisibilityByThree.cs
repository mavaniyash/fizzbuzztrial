﻿namespace FizzBuzz.Business.Service
{
    using global::FizzBuzz.Business.Interface;
    using global::FizzBuzz.Common.Helpers;

    /// <summary>
    /// Class for divisibility by three checking.
    /// </summary>
    public class CheckDivisibilityByThree : ILogicService
    {
        private readonly ICheckDayOfWeek checkDayOfWeek;

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckDivisibilityByThree"/> class.
        /// </summary>
        /// <param name="checkDayOfWeek">Interface for ICheckDayOfWeek.</param>
        public CheckDivisibilityByThree(ICheckDayOfWeek checkDayOfWeek)
        {
            this.checkDayOfWeek = checkDayOfWeek;
        }

        /// <summary>
        /// Method used to check if the number if divisibility by three.
        /// </summary>
        /// <param name="numberToCheck">Number to check.</param>
        /// <returns>True/False.</returns>
        public bool CheckDivisibility(int numberToCheck)
        {
            return numberToCheck % Constants.FizzNumber == 0;
        }

        /// <summary>
        /// Method used to return the generated output.
        /// </summary>
        /// <returns>Generated output.</returns>
        public string Output()
        {
            return this.checkDayOfWeek.IsDayMatched() ? Constants.Wizz : Constants.Fizz;
        }
    }
}
