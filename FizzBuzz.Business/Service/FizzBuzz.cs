﻿namespace FizzBuzz.Business.Service
{
    using System.Collections.Generic;
    using System.Linq;
    using global::FizzBuzz.Business.Interface;
    using global::FizzBuzz.Models;

    /// <summary>
    /// Class for fizz buzz generation logic service.
    /// </summary>
    public class FizzBuzz : IFizzBuzz
    {
        private readonly IEnumerable<ILogicService> fizzBuzzLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzz"/> class.
        /// </summary>
        /// <param name="logicService">Interface for ILogicService</param>
        public FizzBuzz(IList<ILogicService> logicService)
        {
            this.fizzBuzzLogic = logicService;
        }

        /// <summary>
        /// This method is used to get the list of numbers for the fizz buzz.
        /// </summary>
        /// <param name="input">Input number.</param>
        /// <returns>List of generated number.</returns>
        public List<FizzBuzzModel> GetNumbers(int input)
        {
            List<FizzBuzzModel> numberList = new List<FizzBuzzModel>();

            // Loop throght the numbers starting from 1 to input number
            for (int i = 1; i <= input; i++)
            {
                // Check for the last generated / matched logic which is divisable by the number in loop
                var triggeredLogic = this.fizzBuzzLogic.LastOrDefault(l => l.CheckDivisibility(i));

                // If any logic found
                if (triggeredLogic != null)
                {
                    // Then get the output from the matched / triggerred logic
                    numberList.Add(new FizzBuzzModel() { Value = triggeredLogic.Output() });
                }
                else
                {
                    // If not, then just print the current number
                    numberList.Add(new FizzBuzzModel() { Value = i.ToString() });
                }
            }

            return numberList;
        }
    }
}
