﻿namespace FizzBuzz.Business.Service
{
    using System;
    using global::FizzBuzz.Business.Interface;

    /// <summary>
    /// Class for checking day of week.
    /// </summary>
    public class CheckDayOfWeek : ICheckDayOfWeek
    {
        private readonly string dayOfWeek;

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckDayOfWeek"/> class.
        /// </summary>
        /// <param name="dayToCheck">DayToCheck.</param>
        public CheckDayOfWeek(string dayToCheck)
        {
            this.dayOfWeek = dayToCheck;
        }

        /// <summary>
        /// Method used to check if the current day matched with the day of week.
        /// </summary>
        /// <returns>True/False.</returns>
        public bool IsDayMatched()
        {
            return DateTime.Now.DayOfWeek.ToString().Equals(this.dayOfWeek, StringComparison.OrdinalIgnoreCase);
        }
    }
}
