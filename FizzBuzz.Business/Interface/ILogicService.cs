﻿namespace FizzBuzz.Business.Interface
{
    /// <summary>
    /// Interface for fizz buzz logic.
    /// </summary>
    public interface ILogicService
    {
        /// <summary>
        /// Method used to check if a number if divisible or not.
        /// </summary>
        /// <param name="numberToCheck">Number to check.</param>
        /// <returns>True/False.</returns>
        bool CheckDivisibility(int numberToCheck);

        /// <summary>
        /// Method to return the generated output.
        /// </summary>
        /// <returns>Generated output.</returns>
        string Output();
    }
}
