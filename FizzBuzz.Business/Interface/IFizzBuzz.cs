﻿namespace FizzBuzz.Business.Interface
{
    using System.Collections.Generic;
    using FizzBuzz.Models;

    /// <summary>
    /// Interface for Fizz Buzz.
    /// </summary>
    public interface IFizzBuzz
    {
        /// <summary>
        /// Method to get the list of generated numbers.
        /// </summary>
        /// <param name="number">Input number.</param>
        /// <returns>List of generated numbers.</returns>
        List<FizzBuzzModel> GetNumbers(int number);
    }
}
