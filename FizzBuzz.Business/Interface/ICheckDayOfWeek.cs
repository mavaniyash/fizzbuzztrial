﻿namespace FizzBuzz.Business.Interface
{
    /// <summary>
    /// Interface for Checking Day of the week.
    /// </summary>
    public interface ICheckDayOfWeek
    {
        /// <summary>
        /// Method to match the day.
        /// </summary>
        /// <returns>True/False.</returns>
        bool IsDayMatched();
    }
}
