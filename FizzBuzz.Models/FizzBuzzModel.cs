﻿namespace FizzBuzz.Models
{
    /// <summary>
    /// Class for fizz buzz model.
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Gets or sets or set value.
        /// </summary>
        public string Value { get; set; }
    }
}
