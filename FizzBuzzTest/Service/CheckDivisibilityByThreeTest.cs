﻿namespace FizzBuzzTest.Service
{
    using FizzBuzz.Business.Interface;
    using FizzBuzz.Business.Service;
    using FizzBuzz.Common.Helpers;
    using Moq;
    using Xunit;

    /// <summary>
    /// Class for testing divisibility by three.
    /// </summary>
    public class CheckDivisibilityByThreeTest
    {
        private readonly ILogicService checkDivisibilityByThree;
        private readonly Mock<ICheckDayOfWeek> mockCheckDayOfWeek;

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckDivisibilityByThreeTest"/> class.
        /// </summary>
        public CheckDivisibilityByThreeTest()
        {
            this.mockCheckDayOfWeek = new Mock<ICheckDayOfWeek>();
            this.checkDivisibilityByThree = new CheckDivisibilityByThree(this.mockCheckDayOfWeek.Object);
        }

        /// <summary>
        /// Test method used to check if number is divisibility by three.
        /// </summary>
        /// <param name="numToCheck">Number to check.</param>
        /// <param name="expectedResult">Expected result.</param>
        [Theory]
        [InlineData(1, false)]
        [InlineData(2, false)]
        [InlineData(3, true)]
        [InlineData(5, false)]
        [InlineData(9, true)]
        [InlineData(10, false)]
        [InlineData(11, false)]
        [InlineData(12, true)]
        [InlineData(14, false)]
        [InlineData(15, true)]
        public void CheckDivisibilityByThree_CheckDivisibility_Valid(int numToCheck, bool expectedResult)
        {
            // Act
            bool output = this.checkDivisibilityByThree.CheckDivisibility(numToCheck);

            // Assert
            Assert.Equal(expectedResult, output);
        }

        /// <summary>
        /// Test method to check if divisibility by three.
        /// </summary>
        /// <param name="fizzWizzCheck">True/False.</param>
        /// <param name="expectedResult">Expected Result.</param>
        [Theory]
        [InlineData(true, Constants.Wizz)]
        [InlineData(false, Constants.Fizz)]
        public void CheckDivisibilityByThree_Output_FizzWizz(bool fizzWizzCheck, string expectedResult)
        {
            // Arrange
            this.mockCheckDayOfWeek.Setup(x => x.IsDayMatched()).Returns(fizzWizzCheck);

            // Act
            string resultOutput = this.checkDivisibilityByThree.Output();

            // Assert
            Assert.Equal(expectedResult, resultOutput);
        }

        /// <summary>
        /// Test method to check if divisibility by three.
        /// </summary>
        /// <param name="fizzWizzCheck">True/False.</param>
        /// <param name="expectedResult">Expected result.</param>
        [Theory]
        [InlineData(false, Constants.Wizz)]
        [InlineData(true, Constants.Fizz)]
        public void CheckDivisibilityByThree_Output_FizzWizzNotEqual(bool fizzWizzCheck, string expectedResult)
        {
            // Arrange
            this.mockCheckDayOfWeek.Setup(x => x.IsDayMatched()).Returns(fizzWizzCheck);

            // Act
            string resultOutput = this.checkDivisibilityByThree.Output();

            // Assert
            Assert.NotEqual(expectedResult, resultOutput);
        }
    }
}
