﻿namespace FizzBuzzTest.Service
{
    using FizzBuzz.Business.Interface;
    using FizzBuzz.Business.Service;
    using FizzBuzz.Common.Helpers;
    using Moq;
    using Xunit;

    /// <summary>
    /// Class for check divisibility by five.
    /// </summary>
    public class CheckDivisibilityByFiveTest
    {
        private readonly ILogicService checkDivisibilityByFive;
        private readonly Mock<ICheckDayOfWeek> mockCheckDayOfWeek;

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckDivisibilityByFiveTest"/> class.
        /// </summary>
        public CheckDivisibilityByFiveTest()
        {
            this.mockCheckDayOfWeek = new Mock<ICheckDayOfWeek>();
            this.checkDivisibilityByFive = new CheckDivisibilityByFive(this.mockCheckDayOfWeek.Object);
        }

        /// <summary>
        /// Check divisibility by five test.
        /// </summary>
        /// <param name="numToCheck">Number to check.</param>
        /// <param name="expectedResult">True/False.</param>
        [Theory]
        [InlineData(1, false)]
        [InlineData(2, false)]
        [InlineData(3, false)]
        [InlineData(5, true)]
        [InlineData(9, false)]
        [InlineData(10, true)]
        [InlineData(11, false)]
        [InlineData(12, false)]
        [InlineData(14, false)]
        [InlineData(15, true)]
        public void CheckDivisibilityByFive_CheckDivisibility_Valid(int numToCheck, bool expectedResult)
        {
            // Act
            bool output = this.checkDivisibilityByFive.CheckDivisibility(numToCheck);

            // Assert
            Assert.Equal(expectedResult, output);
        }

        /// <summary>
        /// Check divisibility by five test.
        /// </summary>
        /// <param name="buzzWuzzCheck">True/False.</param>
        /// <param name="expectedResult">Buzz/Wuzz.</param>
        [Theory]
        [InlineData(true, Constants.Wuzz)]
        [InlineData(false, Constants.Buzz)]
        public void CheckDivisibilityByFive_Output_BuzzWuzz(bool buzzWuzzCheck, string expectedResult)
        {
            // Arrange
            this.mockCheckDayOfWeek.Setup(x => x.IsDayMatched()).Returns(buzzWuzzCheck);

            // Act
            string resultOutput = this.checkDivisibilityByFive.Output();

            // Assert
            Assert.Equal(expectedResult, resultOutput);
        }

        /// <summary>
        /// Test method to check divisibility by five.
        /// </summary>
        /// <param name="buzzWuzzCheck">True/False.</param>
        /// <param name="expectedResult">Buzz/Wuzz.</param>
        [Theory]
        [InlineData(false, Constants.Wuzz)]
        [InlineData(true, Constants.Buzz)]
        public void CheckDivisibilityByFive_Output_BuzzWuzzNotEqual(bool buzzWuzzCheck, string expectedResult)
        {
            // Arrange
            this.mockCheckDayOfWeek.Setup(x => x.IsDayMatched()).Returns(buzzWuzzCheck);

            // Act
            string resultOutput = this.checkDivisibilityByFive.Output();

            // Assert
            Assert.NotEqual(expectedResult, resultOutput);
        }
    }
}
