﻿namespace FizzBuzzTest.Service
{
    using FizzBuzz.Business.Interface;
    using FizzBuzz.Business.Service;
    using FizzBuzz.Common.Helpers;
    using Moq;
    using Xunit;

    /// <summary>
    /// Class for testing divisibility by three and five.
    /// </summary>
    public class CheckDivisibilityByThreeAndFiveTest
    {
        private readonly Mock<ICheckDayOfWeek> mockCheckDayOfWeek;
        private readonly ILogicService checkDivisibilityByThreeAndFive;

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckDivisibilityByThreeAndFiveTest"/> class.
        /// </summary>
        public CheckDivisibilityByThreeAndFiveTest()
        {
            this.mockCheckDayOfWeek = new Mock<ICheckDayOfWeek>();
            this.checkDivisibilityByThreeAndFive = new CheckDivisibilityByThreeAndFive(this.mockCheckDayOfWeek.Object);
        }

        /// <summary>
        /// Test method to check number divisibility by three and five.
        /// </summary>
        /// <param name="numToCheck">Number to check.</param>
        /// <param name="expectedResult">True/False.</param>
        [Theory]
        [InlineData(1, false)]
        [InlineData(2, false)]
        [InlineData(3, false)]
        [InlineData(5, false)]
        [InlineData(9, false)]
        [InlineData(10, false)]
        [InlineData(11, false)]
        [InlineData(12, false)]
        [InlineData(14, false)]
        [InlineData(15, true)]
        [InlineData(20, false)]
        [InlineData(30, true)]
        public void CheckDivisibilityByThreeAndFive_CheckDivisibility_Valid(int numToCheck, bool expectedResult)
        {
            // Act
            bool output = this.checkDivisibilityByThreeAndFive.CheckDivisibility(numToCheck);

            // Assert
            Assert.Equal(expectedResult, output);
        }

        /// <summary>
        /// Test method to check divisibility by three and five.
        /// </summary>
        /// <param name="fizzBuzzCheck">True or False.</param>
        /// <param name="expectedResult">Fizz Wizz.</param>
        [Theory]
        [InlineData(false, Constants.Fizz + " " + Constants.Buzz)]
        [InlineData(true, Constants.Wizz + " " + Constants.Wuzz)]
        public void CheckDivisibilityByThreeAndFive_Output_FizzBuzz(bool fizzBuzzCheck, string expectedResult)
        {
            // Arrange
            this.mockCheckDayOfWeek.Setup(x => x.IsDayMatched()).Returns(fizzBuzzCheck);

            // Act
            string resultOutput = this.checkDivisibilityByThreeAndFive.Output();

            // Assert
            Assert.Equal(expectedResult, resultOutput);
        }

        /// <summary>
        /// Check divisibility by three and five.
        /// </summary>
        /// <param name="fizzBuzzCheck">True/False.</param>
        /// <param name="expectedResult">Fizz Wizz.</param>
        [Theory]
        [InlineData(true, Constants.Fizz + " " + Constants.Buzz)]
        [InlineData(false, Constants.Wizz + " " + Constants.Wuzz)]
        public void CheckDivisibilityByThreeAndFive_Output_FizzBuzzNotEqual(bool fizzBuzzCheck, string expectedResult)
        {
            // Arrange
            this.mockCheckDayOfWeek.Setup(x => x.IsDayMatched()).Returns(fizzBuzzCheck);

            // Act
            string resultOutput = this.checkDivisibilityByThreeAndFive.Output();

            // Assert
            Assert.NotEqual(expectedResult, resultOutput);
        }
    }
}
