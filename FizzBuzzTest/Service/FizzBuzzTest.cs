﻿namespace FizzBuzzTest.Service
{
    using System.Collections.Generic;
    using FizzBuzz.Business.Interface;
    using FizzBuzz.Common.Helpers;
    using Moq;
    using Xunit;

    /// <summary>
    /// Class for fizz buzz test.
    /// </summary>
    public class FizzBuzzTest
    {
        private readonly Mock<ILogicService> mockLogicService;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzTest"/> class.
        /// </summary>
        public FizzBuzzTest()
        {
            this.mockLogicService = new Mock<ILogicService>();
        }

        /// <summary>
        /// Test method to check if number is not divisible by 3 or 5.
        /// </summary>
        /// <param name="inputNumber">Number to check.</param>
        /// <param name="expectedResult">Expected result.</param>
        [Theory]
        [InlineData(1, "1")]
        [InlineData(2, "2")]
        [InlineData(4, "4")]
        [InlineData(7, "7")]
        [InlineData(11, "11")]
        public void FizzBuzz_WhenNotDivisibleBy3or5_ReturnNumber(int inputNumber, string expectedResult)
        {
            // Arrange
            this.mockLogicService.Setup(x => x.CheckDivisibility(inputNumber)).Returns(false);

            this.mockLogicService.Setup(y => y.Output()).Returns(string.Empty);

            var fizzBuzzService = new FizzBuzz.Business.Service.FizzBuzz(new List<ILogicService>() { this.mockLogicService.Object });

            // Act
            var fizzBuzzResult = fizzBuzzService.GetNumbers(inputNumber);

            // Assert
            Assert.Equal(fizzBuzzResult[inputNumber - 1].Value, expectedResult);
        }

        /// <summary>
        /// Test method to check if number is three then result should be fizz.
        /// </summary>
        /// <param name="inputNumber">Number to check.</param>
        /// <param name="expectedResult">Expected result.</param>
        [Theory]
        [InlineData(3, Constants.Fizz)]
        public void FizzBuzz_InputIsThree_ReturnFizz(int inputNumber, string expectedResult)
        {
            // Arrange
            this.mockLogicService.Setup(x => x.CheckDivisibility(inputNumber)).Returns(true);

            this.mockLogicService.Setup(y => y.Output()).Returns(Constants.Fizz);

            var fizzBuzzService = new FizzBuzz.Business.Service.FizzBuzz(new List<ILogicService>() { this.mockLogicService.Object });

            // Act
            var fizzBuzzResult = fizzBuzzService.GetNumbers(inputNumber);

            // Assert
            Assert.Equal(fizzBuzzResult[inputNumber - 1].Value, expectedResult);
        }

        /// <summary>
        /// Test method to check if the input number is five then the expected result shoud be buzz.
        /// </summary>
        /// <param name="inputNumber">Number to check.</param>
        /// <param name="expectedResult">Expected result buzz.</param>
        [Theory]
        [InlineData(5, Constants.Buzz)]
        public void FizzBuzz_InputIsFive_ReturnBuzz(int inputNumber, string expectedResult)
        {
            // Arrange
            this.mockLogicService.Setup(x => x.CheckDivisibility(inputNumber)).Returns(true);

            this.mockLogicService.Setup(y => y.Output()).Returns(Constants.Buzz);

            var fizzBuzzService = new FizzBuzz.Business.Service.FizzBuzz(new List<ILogicService>() { this.mockLogicService.Object });

            // Act
            var fizzBuzzResult = fizzBuzzService.GetNumbers(inputNumber);

            // Assert
            Assert.Equal(fizzBuzzResult[inputNumber - 1].Value, expectedResult);
        }

        /// <summary>
        /// Test method to check if input number is fifteen then the expected result should be Fizz Buzz.
        /// </summary>
        /// <param name="inputNumber">Number to check.</param>
        /// <param name="expectedResult">Expected result.</param>
        [Theory]
        [InlineData(15, Constants.Fizz + " " + Constants.Buzz)]
        public void FizzBuzz_InputIsFifteen_ReturnFizzBuzz(int inputNumber, string expectedResult)
        {
            // Arrange
            this.mockLogicService.Setup(x => x.CheckDivisibility(inputNumber)).Returns(true);

            this.mockLogicService.Setup(y => y.Output()).Returns(Constants.Fizz + " " + Constants.Buzz);

            var fizzBuzzService = new FizzBuzz.Business.Service.FizzBuzz(new List<ILogicService>() { this.mockLogicService.Object });

            // Act
            var fizzBuzzResult = fizzBuzzService.GetNumbers(inputNumber);

            // Assert
            Assert.Equal(fizzBuzzResult[inputNumber - 1].Value, expectedResult);
        }
    }
}
