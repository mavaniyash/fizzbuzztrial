﻿namespace FizzBuzz.Common.Helpers
{
    /// <summary>
    /// Class which manages constants for the system.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Fizz constant.
        /// </summary>
        public const string Fizz = "fizz";

        /// <summary>
        /// Buzz constant.
        /// </summary>
        public const string Buzz = "buzz";

        /// <summary>
        /// Wizz constant.
        /// </summary>
        public const string Wizz = "wizz";

        /// <summary>
        /// Wuzz constant.
        /// </summary>
        public const string Wuzz = "wuzz";

        /// <summary>
        /// Constant for Max Page Size for fizz buzz list.
        /// </summary>
        public const int MaxPageSize = 20;

        /// <summary>
        /// Constant number for fizz check.
        /// </summary>
        public const int FizzNumber = 3;

        /// <summary>
        /// Constant number for buzz check.
        /// </summary>
        public const int BuzzNumber = 5;
    }
}
